# Hungarian translation of debian-installer.
# Copyright (C) 2003 THE debian-installer'S COPYRIGHT HOLDER
# This file is distributed under the same license as the debian-installer package.
# VERÓK István <vi@fsf.hu>, 2003.
# Translators, please read /usr/share/doc/po-debconf/README-trans
# or http://www.debian.org/intl/l10n/po-debconf/README-trans
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: base-config\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-10-13 11:05+0200\n"
"PO-Revision-Date: 2004-04-02 09:40+0200\n"
"Last-Translator: VERÓK István <vi@fsf.hu>\n"
"Language-Team: Debian Hungarian Localization Team <debian-l10n-"
"hungarian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../base-config:42
msgid "Configuring the base system..."
msgstr "Alaprendszer konfigurálása folyamatban..."

#: ../lib/menu/shell:3
msgid "Type 'exit' to return to base-config."
msgstr "A base-config programba az 'exit' utasítás kiadásával léphet vissza."

#. translators: there MUST be a comma followed by a space at the end
#. of your translation, or things will break!
#: ../apt-setup:140
msgid "enter information manually, "
msgstr "információk megadása kézzel, "

#: ../apt-setup:297
msgid "Scanning CD, this will take a minute."
msgstr "CD átvizsgálása, ez egy kis időt vesz igénybe."

#. Several notes for translators:
#. * The singular form is only there to make use of plural forms;
#. * If package count is 0, a separate error message should be displayed.
#. * This sentence is part of a longer paragraph, full stop is omitted here.
#. * "${PACKAGE_COUNT}" must not be translated.
#: ../apt-setup:381
#, sh-format
msgid ""
"The Debian package management tool, apt, is now configured, and can install "
"${PACKAGE_COUNT} package"
msgid_plural ""
"The Debian package management tool, apt, is now configured, and can install "
"${PACKAGE_COUNT} packages"
msgstr[0] ""
"A Debian csomagkezelő eszközének (az apt-nak) konfigurálása megtörtént, most "
"már telepítheti az összesen ${PACKAGE_COUNT} csomag bármelyikét"

#: ../apt-setup:478
msgid ""
"You probably used a CD to install the Debian base system, but it is not "
"currently in the drive. You should probably just insert it and select \"cdrom"
"\"."
msgstr ""
"Debian-rendszerét CD-ről telepíthette, de az most nincsen a meghajtóban. "
"Helyezze be és válassza a cdrom pontot."

#: ../apt-setup:730 ../apt-setup:813 ../apt-setup:938 ../apt-setup:951
#: ../apt-setup:1098 ../apt-setup:1128
msgid "Testing apt sources..."
msgstr "Apt-források vizsgálata folyamatban ..."

#~ msgid "Updating \"available\" file..."
#~ msgstr "'available' fájl frissítése folyamatban..."

#~ msgid "dselect - old package selector (experts only)"
#~ msgstr "dselect - régi csomagkiválasztó felület (csak hozzáértőknek)"

#~ msgid "aptitude - pick tasks or drill down to individual packages"
#~ msgstr "aptitude - az osztályozott csomagokból akár egyenként is válogathat"

#~ msgid "tasksel - quickly choose from predefined collections of software"
#~ msgstr ""
#~ "tasksel - előre összeállított csomagcsoportokból lehet csak választani"

#~ msgid "nothing - you may manually run apt-get or any of the above later"
#~ msgstr "semmi - az apt-get parancs (vagy a fentiek) futtatása majd később"

#~ msgid "Testing apt sources ..."
#~ msgstr "Apt-források vizsgálata folyamatban ..."

#~ msgid "package"
#~ msgstr "csomag"

#~ msgid "packages"
#~ msgstr "csomag"

#~ msgid "enter information manually"
#~ msgstr "információk megadása kézzel"

#~ msgid "Debian System Configuration"
#~ msgstr "Debian-rendszer konfigurálása"

#~ msgid "Apt Configuration"
#~ msgstr "Apt konfigurálása"

#~ msgid "Time Zone Configuration"
#~ msgstr "Időzóna konfigurálása"
