# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/lt.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# Lithuanian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Kęstutis Biliūnas <kebil@kaunas.init.lt>, 2004, 2005.
# Marius Gedminas <mgedmin@b4net.lt>, 2004.
# Darius Skilinskas <darius10@takas.lt>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: lt\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-11-28 11:36+0100\n"
"PO-Revision-Date: 2005-07-06 23:56+0300\n"
"Last-Translator: Kęstutis Biliūnas <kebil@kaunas.init.lt>\n"
"Language-Team: Lithuanian <komp_lt@konferencijos.lt>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10\n"
"X-Poedit-Language: Lithuanian\n"
"X-Poedit-Country: LITHUANIA\n"

#. Type: text
#. Description
#: ../main-menu.templates:3
#, fuzzy
msgid "Guadalinex Mini installer main menu"
msgstr "Guadalinex įdiegiklio pagrindinis meniu"

#. Type: select
#. Description
#: ../main-menu.templates:9
msgid "Choose the next step in the install process:"
msgstr "Pasirinkite tolesnį įdiegimo žingsnį:"

#. Type: select
#. Description
#: ../main-menu.templates:9
#, fuzzy
msgid "This is the main menu for the Guadalinex Mini installer."
msgstr "Čia pagrindinis Guadalinex įdiegiklio meniu."

#. Type: error
#. Description
#: ../main-menu.templates:14
msgid "Installation step failed"
msgstr "Įdiegimo žingsnis nepavyko"

#. Type: error
#. Description
#: ../main-menu.templates:14
msgid ""
"An installation step failed. You can try to run the failing item again from "
"the menu, or skip it and choose something else. The failing step is: ${ITEM}"
msgstr ""
"Šis įdiegimo žingsnis nepavyko. Galite bandyti kartoti nepavykusį žingsnį "
"per meniu, arba jį praleisti pasirinkdami kurį nors kitą. Nepavykęs "
"žingsnis: ${ITEM}"

#. Type: select
#. Description
#: ../main-menu.templates:23
msgid "Choose an installation step:"
msgstr "Pasirinkite įdiegimo žingsnį:"

#. Type: select
#. Description
#: ../main-menu.templates:23
msgid ""
"This installation step depends on one or more other steps that have not yet "
"been performed."
msgstr ""
"Šis diegimo žingsnis priklauso nuo vieno ar kelių kitų, kol kas dar "
"neatliktų žingsnių."

#~ msgid "Debian installer main menu"
#~ msgstr "Debian'o įdiegiklio pagrindinis meniu"

#~ msgid "This is the main menu for the Debian installer."
#~ msgstr "Čia pagrindinis Debian'o įdiegiklio meniu."
